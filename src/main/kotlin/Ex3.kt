import kotlinx.coroutines.delay
import java.io.File
import kotlin.system.measureTimeMillis

suspend fun main(){
    val lyrics = File("src/main/kotlin/song").readLines()
    var timeTotal = 0L

    for (i in lyrics){
        val time = measureTimeMillis {
            delay(300)
            println(i)
        }
        timeTotal += time
    }

    println("\nSong length: ${(timeTotal/1000)/60} minutes ${(timeTotal/1000)%60} seconds")
}