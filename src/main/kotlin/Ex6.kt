import kotlinx.coroutines.*

suspend fun main() {
    val horses = listOf<Horse>(Horse("Pol Agustina"), Horse("Cheetos"), Horse("McPoyle"), Horse("Sprocket"))

    runBlocking {
        horses.forEach { horse ->
            launch { horse.run() }
        }
    }
}

class Horse(private val name: String){
    private val randomTime = listOf<Long>(500, 1000, 2000, 3000, 4000, 5000)

    suspend fun run(){
        for (lap in 1..4){
            delay(randomTime.random())
            when (lap){
                1,2,3 -> println("$name completed lap $lap!")
                4 -> println("$name finished the race!")
            }
        }

    }
}
