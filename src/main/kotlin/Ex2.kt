import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

suspend fun main() {
    withContext(Dispatchers.Default){
        launch {
            println("Hello World!")
            delay(100)
            println("Message 1")
            delay(100)
            println("Message 2")
            delay(100)
            println("Message 3")
            delay(100)
            println("Message 4")
            delay(100)
        }
    }

    println("Finished!")
}
