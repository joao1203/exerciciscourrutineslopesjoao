import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main() {
    //first line to run
    println("The main program is started")
    GlobalScope.launch {
        //third thing to run
        println("Background processing started")
        delay(1000)
        //does not run
        println("Background processing finished")
    }
    //second line to run
    println("The main program continues")
    runBlocking {
        delay(900)
        //fourth line to run
        println("The main program is finished")
    }
}
