import kotlinx.coroutines.delay
import java.util.*
import kotlin.system.measureTimeMillis

suspend fun main(){
    val sc = Scanner(System.`in`)
    val randomNum = (0..10).random()
    var totalTime = 0L

    println("You have 10 seconds to guess the secret number...")

    while (totalTime < 10000){
        val time = measureTimeMillis {
            print("Enter a number: ")
            val userNum = sc.nextInt()
            if (userNum != randomNum){
                println("Wrong number!")
            } else{
                println("You got it!")
                totalTime = 10000
            }
            delay(100)
        }
        totalTime += time
    }

    println("Time is up!")
}