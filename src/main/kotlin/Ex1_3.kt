import kotlinx.coroutines.*

suspend fun main(){
    println("The main program is started")
    doBackgroundWithContext()
    println("The main program continues")
    runBlocking {
        delay(1500)
        println("The main program is finished")
    }
}
suspend fun doBackgroundWithContext(){
    withContext(Dispatchers.Default) {
        launch{
            println("Background processing started")
            delay(1000)
            println("Background processing finished")
        }
    }
}
